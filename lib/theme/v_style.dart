import 'package:flutter/material.dart';

///
///
/// @author  liu xm
/// @date  2022-06-06 12:32
///
class Style {
  static const black = Colors.black;
  static const white = Colors.white;
  static const gray1 = Color(0xfff7f8fa);
  static const gray2 = Color(0xfff2f3f5);
  static const gray3 = Color(0xffebedf0);
  static const gray4 = Color(0xffdcdee0);
  static const gray5 = Color(0xffc8c9cc);
  static const gray6 = Color(0xff969799);
  static const gray7 = Color(0xff646566);
  static const gray8 = Color(0xff323233);
  static const red = Color(0xffee0a24);
  static const blue = Color(0xff1989fa);
  static const yellow = Color(0xffffd21e);
  static const orange = Color(0xffff976a);
  static const orangeDark = Color(0xffed6a0c);
  static const orangeLight = Color(0xfffffbe8);
  static const green = Color(0xff07c160);
  static const transparent = Colors.transparent;

  // Component Colors
  static const textColor = gray8;

  // padding
  static const paddingBase = 4.0;
  static const paddingXs = paddingBase * 2;
  static const paddingSm = paddingBase * 3;
  static const paddingMd = paddingBase * 4;
  static const paddingLg = paddingBase * 6;
  static const paddingXl = paddingBase * 8;

  // Font
  static const fontSizeXs = 10.0;
  static const fontSizeSm = 12.0;
  static const fontSizeMd = 14.0;
  static const fontSizeLg = 16.0;
  static const fontSizeXl = 20.0;
  static const fontWeightBold = FontWeight.w500;

  // interval
  static const intervalBase = 2.0;
  static const intervalSm = intervalBase * 2;
  static const intervalMd = intervalBase * 3;
  static const intervalLg = intervalBase * 4;
  static const intervalXl = intervalBase * 6;

  // Animation
  static const animationDurationBase = Duration(milliseconds: 500);
  static const animationDurationFast = Duration(milliseconds: 300);
  static const animationDurationSlow = Duration(milliseconds: 1000);

  // Border
  static const borderColor = gray3;
  static const borderWidthBase = 1.0;
  static const borderWidthHair = 0.5;
  static const borderRadiusSm = 2.0;
  static const borderRadiusMd = 4.0;
  static const borderRadiusLg = 8.0;
  static const borderRadiusMax = 999.0;

  // Cell
  static const cellFontSize = fontSizeMd;
  static const cellVerticalPadding = paddingSm;
  static const cellHorizontalPadding = paddingMd;
  static const cellTextColor = textColor;
  static const cellBackgroundColor = white;
  static const cellBorderColor = borderColor;
  static const cellRequiredColor = red;
  static const cellLabelColor = gray6;
  static const cellLabelFontSize = fontSizeSm;
  static const cellValueColor = gray6;
  static const cellIconSize = 16.0;
  static const cellRightIconColor = gray6;
  static const cellLargeVerticalPadding = paddingSm;
  static const cellLargeTitleFontSize = fontSizeLg;
  static const cellLargeLabelFontSize = fontSizeMd;

  // Collapse
  static const collapseItemTransitionDuration = animationDurationBase;
  static const collapseItemContentPadding =
      EdgeInsets.fromLTRB(0, paddingMd, paddingMd, paddingMd);
  static const collapseItemContentMargin = EdgeInsets.only(left: paddingMd);
  static const collapseItemContentFontSize = 13.0;
  static const collapseItemContentTextColor = gray6;
  static const collapseItemContentBackgroundColor = white;
  static const collapseItemTitleDisabledColor = gray5;

  // Dialog
  static const dialogWidth = 320.0;
  static const dialogSmallScreenWidth = 280.0;
  static const dialogFontSize = fontSizeLg;
  static const dialogBorderRadius = 16.0;
  // static const dialogTextColor = textColor;
  static const dialogBackgroundColor = white;
  // static const dialogHeaderFontWeight = fontWeightBold;
  // static const dialogHeaderPadding =
  //     EdgeInsets.fromLTRB(paddingLg, paddingLg, paddingLg, 0);
  static const dialogMessagePadding = paddingLg;
  static const dialogMessageFontSize = fontSizeLg;
  static const dialogHasTitleMessageTextColor = gray7;
  static const dialogHasTitleMessagePaddingTop = paddingSm;
  static const dialogConfirmButtonTextColor = blue;
  static const dialogCancelButtonTextColor = textColor;
  static const dialogButtonHeight = 50.0;

  // Divider
  static const dividerTextColor = gray6;
  static const dividerFontSize = fontSizeMd;
  static const dividerBorderColor = borderColor;
  static const dividerContentPadding = EdgeInsets.symmetric(horizontal: 10);
  static const dividerContentLeftWidth = 32.0;
  static const dividerContentRightWidth = 32.0;

  // Field
  static const fieldLabelWidth = 80.0;
  static const fieldMinHeight = 30.0;
  static const fieldFontSize = fontSizeMd;
  static const fieldPadding =
      EdgeInsets.symmetric(horizontal: paddingMd, vertical: 10);
  static const fieldInputPadding = EdgeInsets.symmetric(vertical: 5);
  static const fieldInputBackgroundColor = white;
  static const fieldInputTextColor = textColor;
  static const fieldInputCursorWidth = 1.0;
  static const fieldRequiredColor = red;
  static const fieldInputErrorTextColor = red;
  static const fieldInputDisabledTextColor = gray6;
  static const fieldPlaceholderTextColor = gray6;
  static const fieldIconSize = 16.0;
  static const fieldClearIconSize = 16.0;
  static const fieldClearIconColor = gray5;
  static const fieldRightIconColor = gray6;
  static const fieldErrorMessageColor = red;
  static const fieldErrorMessageTextSize = 12.0;
  static const fieldWordLimitColor = gray7;
  static const fieldWordLimitFontSize = fontSizeSm;

  // NumberKeyBoard
  static const numberKeyboardBackgroundColor = white;
  static const numberKeyboardTitleTextColor = gray7;
  static const numberKeyboardTitlePadding =
  EdgeInsets.symmetric(vertical: paddingXs, horizontal: paddingMd);
  static const numberKeyboardTitleFontSize = fontSizeMd;
  static const numberKeyboardCloseColor = blue;
  static const numberKeyboardCloseFontSize = fontSizeMd;
  static const numberKeyboardNumSpacing = 0.6;
  static const numberKeyboardKeyBackground = gray3;
  static const numberKeyboardKeyFontSize = 24.0;
  static const numberKeyboardDeleteFontSize = fontSizeLg;

  // PasswordInput
  static const passwordInputHeight = 52.0;
  static const passwordInputMargin =
  EdgeInsets.symmetric(horizontal: paddingMd);
  static const passwordInputFontSize = 20.0;
  static const passwordInputColor = textColor;
  static const passwordInputGutter = 6.0;
  static const passwordInputBorderRadius = 6.0;
  static const passwordInputBackgroundColor = white;
  static const passwordInputInfoColor = gray6;
  static const passwordInputInfoFontSize = fontSizeMd;

  // Search
  static const searchBackgroundColor = white;
  static const searchInputBackgroundColor = gray1;
  static const searchPadding =
      EdgeInsets.symmetric(vertical: 10, horizontal: paddingSm);
  static const searchInputFontSize = fontSizeLg;
  static const searchInputPlaceholderColor = gray6;
  static const searchInputColor = textColor;
  static const searchLabelPadding = EdgeInsets.symmetric(horizontal: 5);
  static const searchLabelColor = textColor;
  static const searchLabelFontSize = fontSizeXl;
  static const searchLeftIconColor = gray6;
  static const searchActionPadding = EdgeInsets.only(left: paddingXs);
  static const searchActionTextColor = textColor;
  static const searchActionFontSize = fontSizeMd;
}
